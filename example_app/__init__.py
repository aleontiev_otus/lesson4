import os

from liteWSGI import server, config, log_handler
from liteWSGI import render_template, jsonify

config.set_prop(
    'dir_main',
    os.path.dirname(os.path.abspath(__file__)))

log_handler.enable_debug()

application = server()
app = application


@app.route('/')
def index(*args):
    return 'Hello World'


@app.route('/about')
def about(*args):
    return 'About'


@app.route('/j2')
def j2(*args):
    return render_template('index.j2', {
        'main_text': 'Jinja test'
    })


@app.route('/json')
def json_test(req):
    return jsonify(req)


@app.render_exception(404)
def _404():
    return render_template('errors/404.j2')
