from liteWSGI.core import log_handler
from liteWSGI.core.config import Config

config = Config()

log = log_handler(
    config.config['debug'],
    config.config['log_format_string']
)


from liteWSGI.core import LiteWSGI  # noqa: E402
from liteWSGI.core.renders import render_template, jsonify  # noqa: F401,E402

__version__ = "0.0.1"
server = LiteWSGI
