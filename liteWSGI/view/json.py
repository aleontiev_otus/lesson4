import json

from liteWSGI.view.base import BaseView


class JSONEncoderWithoutTypeError(json.JSONEncoder):
    """
    JSONEncoderWithoutTypeError - Implementation of the json class.JSONEncoder.
    Overrides the behavior in cases where the object type is different from the base type.
    If the object value cannot be converted to a json object, the value is replaced with null
    """

    def default(self, o):
        try:
            iterable = iter(o)
        except TypeError:
            pass
        else:
            try:
                return list(iterable)
            except:  # noqa: E722
                pass
            # Let the base class default method raise the TypeError
        return


class JsonView(BaseView):
    alias = 'json'
    content_type = "application/json"

    def render(self, data, options=False):
        """
        Converting data to a string

        :param str|obj data: The data that will be converted to json
        :param options: False, don't use
        :return: bytes
        """

        return json.dumps(data, cls=JSONEncoderWithoutTypeError).encode()
