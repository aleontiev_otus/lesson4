from .base import BaseView
from .json import JsonView  # noqa: F401
from .text import PlainView  # noqa: F401
from .jinja2 import TemplateView  # noqa: F401

views = {}
for view in BaseView.__subclasses__():
    view = view()
    views[view.alias] = view
