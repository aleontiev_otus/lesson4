class BaseView(object):
    alias = 'base'

    def __init__(self):
        from liteWSGI import log
        self.log = log

    def render(self, data, options=False):
        raise NotImplemented
