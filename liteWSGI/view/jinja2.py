from jinja2 import Environment, FileSystemLoader, select_autoescape, TemplateNotFound

from liteWSGI.view.base import BaseView


class TemplateView(BaseView):
    alias = 'jinja'
    content_type = "text/html"

    jinja_options = dict(
        extensions=['jinja2.ext.autoescape', 'jinja2.ext.with_']
    )

    def __init(self):
        """
        Initializing the environment for the template engine

        :return: None
        """

        from liteWSGI import config
        self.config = config.config
        self.log.debug('Init env for jinja2, path templates - {path}'.format(
            path=[
                f"{self.config['dir_main']}/{self.config['dir_template']}",
                self.config['dir_main']
            ]
        ))

        self.j2_env = Environment(
            loader=FileSystemLoader([
                f"{self.config['dir_main']}/{self.config['dir_template']}",
                self.config['dir_main']
            ]),
            autoescape=select_autoescape(['html', 'xml'])
        )

    def render(self, data, options=False):
        """
        Check and render template to string

        :param str data: path to template
        :param dict options: definitions for template
        :return: bytes
        """

        self.__init()
        if not options:
            options = {}

        try:
            template = self.j2_env.get_or_select_template(data)
        except TemplateNotFound:
            self.log.debug(f'Jinja2 not found template {data}')
            raise

        return template.render(options).encode()
