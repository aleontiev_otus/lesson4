from liteWSGI.view.base import BaseView


class PlainView(BaseView):
    alias = 'plain'
    content_type = "text/plain"

    def render(self, data, options=False):
        """
        Check type for data, support only text string. And encode string to bytes.

        :param str data: String with text for render
        :param bool options: Don't use
        :return: bytes|bool
        """
        self.log.debug(f'Render text view - {data}')

        if type(data) is str:
            return data.encode()

        return False
