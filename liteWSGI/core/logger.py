import logging
import sys


class Logger:
    LOG = logging.getLogger()

    def __call__(self, debug=False, log_format=False):
        self.init_logger(debug, log_format)
        return self.LOG

    def init_logger(self, debug=False, log_format=False):
        """Initialize the logger
        :param debug: debug mode
        """

        self.LOG.handlers = []
        log_level = logging.INFO
        if debug:
            log_level = logging.DEBUG

        logging.captureWarnings(True)
        self.LOG.setLevel(log_level)
        handler = logging.StreamHandler(sys.stderr)
        formatter = logging.Formatter(log_format)
        handler.setFormatter(formatter)

        self.LOG.addHandler(handler)
        self.LOG.debug("logging initialized")

    def enable_debug(self):
        self.LOG.setLevel(logging.DEBUG)
