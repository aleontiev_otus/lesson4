class NotFound(BaseException):
    http_code = 404
    pass


class NotFoundView(BaseException):
    http_code = 502
    pass


class HTTPCodeError(BaseException):
    http_code = 502
    pass
