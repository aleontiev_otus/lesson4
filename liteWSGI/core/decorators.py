def route(obj, path, methods=[]):
    def route_decorators(func):
        def wrapper():
            obj.add_rule_url(methods, path, func)
        return wrapper()

    return route_decorators


def render_exception(obj, code):
    def exception_decorators(func):
        def wrapper():
            obj.register_exception(code, func)
        return wrapper()

    return exception_decorators
