from liteWSGI.view import views
from liteWSGI.core.exceptions import NotFoundView


def jsonify(data, status_code=200):
    """
    Prepare render object|str for json response

    :param data: Data for convert to json
    :param status_code: return http status code
    :return: tuple
    """

    if 'json' not in views:
        raise NotFoundView()

    return (status_code,
            views['json'].render(data),
            {'content-type': views['json'].content_type})


def render_template(template_path, options=False, status_code=200):
    """
    Prepare for render template, compile and make options

    :param str template_path: path to template
    :param dict options: variables for template
    :param int status_code: set http status code
    :return: tuple
    """

    if 'jinja' not in views:
        raise NotFoundView()

    return (status_code,
            views['jinja'].render(template_path, options),
            {'content-type': views['jinja'].content_type})
