from liteWSGI.core import constants


class Config(object):

    def __init__(self):
        self._config = {}
        self._config['default_methods'] = constants.route_config['methods']
        self._config['log_format_string'] = constants.log_format
        self._config['dir_main'] = constants.dir_main
        self._config['dir_template'] = constants.dir_template
        self._config['debug'] = False

    @property
    def config(self):
        return self._config

    def set_prop(self, name, value):
        self._config[name] = value
