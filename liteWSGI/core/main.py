import sys
from http import HTTPStatus

from liteWSGI.core import decorators, response
from liteWSGI.core.exceptions import NotFound


class LiteWSGI:

    route = decorators.route
    render_exception = decorators.render_exception
    url_map = {}

    def __init__(self):
        from liteWSGI import log
        self.log = log

        self.default_methods = ['POST', 'GET']
        self.allow_methods = [
            'GET', 'PATCH', 'POST', 'PUT', 'DELETE', 'OPTIONS'
        ]
        self.default_code = 200
        self.exception_data = {}

    def __check_methods(self, methods):
        """
        Check and filter unsupported http methods
        :param tuple|list methods: HTTP methods
        :return: list
        """

        if type(methods) is not list:
            return []

        return list(filter(lambda x: x in self.allow_methods, methods))

    def __prepare_rule(self, rule):
        """
        Prepare rule. Remove end backslash
        :param str rule: Rule for route
        :return: str
        """
        if rule.endswith('/'):
            rule = rule[0:-1]

        return rule

    def add_rule_url(self, methods, rule, endpoint):
        """
        Add rules for path

        :param tuple|list methods: HTTP methods
        :param str rule: the template for the compliance url
        :param endpoint: callback function, to which the query parameters will be passed
        :return: None
        """
        rule = self.__prepare_rule(rule)

        if not self.__check_methods(methods):
            methods = self.default_methods

        for method in methods:
            if method not in self.url_map:
                self.url_map[method] = {}

            if rule not in self.url_map[method]:
                self.url_map[method][rule] = endpoint
                self.log.debug(f'Add url rule, method: {method}, rule: {rule}, endpoint: {endpoint}')

    def get_endpoint(self, environ):
        """
        Get endpoint for url and current http method

        :param dict environ: request information
        :return: None
        """

        if environ['REQUEST_METHOD'] not in self.url_map:
            raise NotFound()

        rule = self.__prepare_rule(environ['PATH_INFO'])

        if rule not in self.url_map[environ['REQUEST_METHOD']]:
            raise NotFound()

        return self.url_map[environ['REQUEST_METHOD']][rule]

    def handle_exception(self, e):
        """
        Handle exceptions with generated response for exception

        :param tuple e: exception info
        :return: Response
        """

        status_code = getattr(e[0], 'http_code', 502)

        data = str(status_code)
        if status_code in self.exception_data:
            data = self.exception_data[status_code]

        self.log.debug(f'Handle exception {e[0]}, {e[1]}')
        options = {}
        try:
            options = data[2]
        except IndexError:
            pass

        return response.Response(status_code=status_code, data=data[1], options=options)

    def register_exception(self, code, func):
        """
        Register a handler for errors with the specified http status code

        :param code: HTTP status code
        :param func: callback function for generated response
        :return: None
        """

        self.log.debug(f'Registering exception status_code: {code}, func: {func}')

        if not HTTPStatus(code):
            self.log.debug(f'Registering exception error, don\'t valid status code {code}')
            return False

        data = func()
        if type(data) is not tuple:
            data = code, data.encode()

        self.exception_data[code] = data

    def wsgi_app(self, environ, start_response):
        """
        Main wsgi application handler

        :param environ: Request info
        :param start_response: callback function for init response
        :return: tuple
        """

        self.log.debug(f'Get request environ: {environ}')
        try:
            endpoint = self.get_endpoint(environ)
            data = endpoint(environ)
            resp = response.Response()
            resp.make(data)
        except:  # noqa: E722
            resp = self.handle_exception(sys.exc_info())

        start_response(resp.get_code(), resp.get_headers())
        return [resp.data]

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)
