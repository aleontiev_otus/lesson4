import os

log_format = '%(asctime)-15s %(message)s'
route_config = {
    'methods': ['POST', 'GET']
}
dir_main = os.getcwd()
dir_template = 'templates'
