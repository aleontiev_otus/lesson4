from http import HTTPStatus
import inspect

from liteWSGI.core.exceptions import HTTPCodeError


class Response:

    code = 200
    content_type = 'text/plain'
    headers = {}
    charset = 'utf-8'
    data = ''

    def __init__(self, status_code=False, data=False, content_type=False, charset=False, headers=False, options=False):
        from liteWSGI import log
        self.log = log

        self.log.debug(f'Response init: {inspect.signature(self.__init__).parameters}')

        if content_type:
            self.__set_content_type(content_type)

        if charset:
            self.charset = charset

        if headers and headers is dict:
            self.headers = headers

        if data and self.__check_data(data):
            if type(data) is not bytes:
                data = data.encode()

            self.__set_data(data)

        if options and type(options) is dict:
            self.__parse_options(options)

        if status_code and self.__get_http_status(status_code):
            self.code = status_code

    def __check_data(self, data):
        """
        Check data, data for response can be only str or bytes

        :param data: object for data
        :return: bool
        """

        if type(data) in [str, bytes]:
            return True
        return False

    def __set_data(self, d):
        """
        Set data, with log if debug mode

        :param str|bytes d: data for response
        :return: None
        """

        self.log.debug(f'Response - set data: {d}')
        self.data = d

    def __set_content_type(self, t):
        """
        Set content-type, with log if debug mode

        :param str t: content-type for response
        :return: None
        """

        self.log.debug(f'Response - set content-type: {t}')
        self.content_type = t

    def __parse_options(self, options):
        """
        Parse options and set headers, with log if debug mode

        :param dict options: Dict with header and other options
        :return: None
        """

        self.log.debug(f'Response - parse options: {options}')

        if 'content-type' in options:
            self.__set_content_type(options['content-type'])

        if 'headers' in options and options['header'] is dict:
            self.headers = options['header']

    def __get_http_status(self, code):
        """
        Check and get text for http status code.

        :param int code: http code
        :return: str|bool
        """
        self.log.debug(f'Get/check http status: {code}')

        try:
            code = HTTPStatus(code)

            return f'{code.value} {code.phrase}'
        except ValueError:
            pass

        return False

    def make(self, data):
        """
        Parse data and make response. Set data and other params.

        :param str|tuple data: Can be text string or tuple. (If data is tuple, can be set status code, data, options)
        :return: self
        """

        self.log.debug(f'Response - make object')
        if type(data) in [tuple, list]:
            http_code = self.__get_http_status(data[0])
            if not http_code:
                raise HTTPCodeError()

            self.data = data[1]
            self.code = data[0]

            if data[2] and type(data[2]) is dict:
                self.__parse_options(data[2])

            return self

        self.data = data.encode()

        self.log.debug(f'Response - make object, data: {data}, result: {self}')
        return self

    def get_cookie(self):
        raise NotImplemented

    def set_cookie(self, name, value):
        raise NotImplemented

    def get_code(self):
        """
        Public method for get status code as string
        :return: str
        """

        return self.__get_http_status(self.code)

    def get_headers(self):
        """
        Convert dict headers to tuple (key, name).

        :return: tuple
        """

        headers = self.headers or {}
        headers['content-type'] = self.content_type
        headers['Accept-Charset'] = self.charset

        return [(k, headers[k]) for k in headers]
