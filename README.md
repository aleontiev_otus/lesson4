# liteWSGI - Framework for work with WSGI protocol (Lite)

## Describe

The library allows you to work with the WSGI Protocol quickly and easily.

### Functionality

- routing, use decorator `@route(<path>)`
- routing for protocol, use decorator `@route(<path>, methods=['POST', 'PATCH'])`
- parsing request (request info return to view function)
- render response:
    - plain, `return <text>`
    - json (use helper liteWSGI.jsonify), `return jsonify(<obj>)`
    - html (use helper liteWSGI.render_template), `return render_template(<path_to_template>, <data_dict>)`
- error handling with the generation of the response (use decorator `@app.render_exception(<http_code>)`)

### Template

The JINJA2 template engine is supported. See documentation for [jinja2](http://jinja.pocoo.org/docs/2.10/).  
By default, templates are searched in the
`main directory and <main path>/templates directory`.  
You set main directory, use config.set_prop(). Example:

```python

import os

from liteWSGI import config // Import server handler

config.set_prop(
    'dir_main',
    os.path.dirname(os.path.abspath(__file__))) // Set main template, where we search templates


```


### Requirements

- Python 3.6
- See in file `requirements.txt`

## Install

- `git clone https://gitlab.com/aleontiev_otus/lesson4.git`
- Install requirements. `pip install -r requirements.txt`

## Quick start

- Install all requirements `pip install -r requirements.txt`
- Run example app `uwsgi --http :9090 --module=example_app  --chdir ./`

> If you use virtualenv, run example app with arg `--virtualenv <path to venv dir>`.
 Example - `uwsgi --http :9090 --module=example_app --virtualenv ./venv/ --chdir ./`

### Example app

The application displays the main features. The url that query browser:

- [/](http://127.0.0.1:9090/) - simple render text, shows the work of the routing
- [/about](http://127.0.0.1:9090/about)  - simple render text, shows the work of the routing
- [/json/](http://127.0.0.1:9090/json/)  - render json object (request info)
- [/j2/](http://127.0.0.1:9090/j2/)  - render j2 template in dir templates/index.j2
- [/any](http://127.0.0.1:9090/any/)  - test for render 404 (use j2 template)

## CLI

Not supported.


## Example

### Use routing


```python
from liteWSGI import server // Import server handler

application = server() // Init application (application use in uwsgi as default module)
app = application // Simple short alias for application

@app.route('/')
def index(*args):
    return 'Hello World'


@app.route('/about')
def about(*args):
    return 'About'

```

### Render templates (jinja2)


> Notice! By default, templates are searched in the `main directory and <main path>/templates directory`.


```python
import os

from liteWSGI import server, config // Import server handler
from liteWSGI import render_template // Import render helpers

config.set_prop(
    'dir_main',
    os.path.dirname(os.path.abspath(__file__))) // Set main template, where we search templates

application = server() // Init application (application use in uwsgi as default module)
app = application // Simple short alias for application

@app.route('/index')
def index(*args):
    return render_template('index.j2', {  // Use helper for render template
        'main_text': 'Jinja test'
    })

```


### Render json

```python

from liteWSGI import server // Import server handler
from liteWSGI import jsonify // Import render helpers

application = server() // Init application (application use in uwsgi as default module)
app = application // Simple short alias for application

@app.route('/index')
def index(req):
    return jsonify(req)

```


### Use handler for error 404

```python

from liteWSGI import server // Import server handler

application = server() // Init application (application use in uwsgi as default module)
app = application // Simple short alias for application

@app.render_exception(404)
def _404():
    return '404 - Ups ... '


```


## Debug

You can enable detailed logging using the log_handler object by calling the method enable_debug.

Example:

```python

import os

from liteWSGI import server, log_handler
log_handler.enable_debug()

```

## Structure

- core: Main functions
- view: Response data output handlers (ex.: json, text plain, etc.)

## Contact

**Author:** Artem Leontiev  
**Contact email:** [artdokxxx@gmail.com]
